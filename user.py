#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import simpy
import random
import numpy as np

from sfc import SFCRequest

class User:

    def __init__(self, id, lifetime, req_time, nets, sfcs, env):
        self.id = id
        self.lifetime = lifetime
        self.req_time = req_time
        self.sfcs = sfcs
        self.nets = nets
        self.env = env
        self.action = env.process(self.run())
        self.requests = []

    def run(self):
        while True:
            if self.env.now > self.lifetime:
                self.env.exit()
            sfc = random.choice(self.sfcs)
            for net in self.nets:
                sfc_req = SFCRequest(sfc, self.env.now, self.env)
                request = (self, sfc_req, self.env.now)
                net.requests.put(request)
            self.requests.append((SFCRequest(sfc, self.env.now, self.env)))
            yield self.env.timeout(self.req_time)
            
            
