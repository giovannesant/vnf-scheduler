#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from itertools import cycle
from vnf import VNF

import heft.core 
import math

class ProposedScheduler:


    def __init__(self):
        self.name = "ProposedScheduler"
        self.node_order = []
        self.cost = {}

    def schedule(self, network, sfc_req):
        schedule = []
        vnfs_to_instantiate = []
        already_instantiated = []
        
        if self.node_order == []:
            self.node_order = cycle(network.nodes)

        self.cost = {}

        for i, vnf in enumerate(sfc_req.sfc.vnfs):

            if network.does_not_fit(sfc_req.sfc.vnfs[i]):
                if network.instance_not_available(sfc_req.sfc.vnfs[i]):
                    raise Exception("Network is full")
#                    network.destroy_lru()
                else:
                    can_execute = False
                    for node in network.nodes:
                        if vnf in node.instantiated_vnfs and node.has_resources(vnf):
                            self.cost[(i+1, node)] = node.ptime[sfc_req.sfc.vnfs[i].type] + node.next_start
                            already_instantiated.append(vnf)
                            can_execute = True
                  
                    if not can_execute:
                        raise Exception("Network is full")

            if vnf not in already_instantiated:
                for node in self.node_order:
                    if node.has_capacity():
                        self.add_instantiation_to_dag(sfc_req, vnf)
                        self.cost[(i+1, node)] = node.ptime[sfc_req.sfc.vnfs[i].type] + node.next_start
                        self.cost[(sfc_req.last_index-1, node)] = node.itime[sfc_req.sfc.vnfs[i].type] + node.next_start
                        node.instantiate(vnf)
                        break

        orders, jobson = heft.core.schedule(sfc_req.dag, network.nodes, self.compcost, self.commcost)

        for node in orders:
            queue_demand = 0
            for event in orders[node]:
                queue_demand += sfc_req.vnfs[event.job-1].queue_demand
            if node.cur_queue_size + queue_demand > node.queue_size:
                raise Exception("Invalid Schedule")

        for node in orders:
            for event in orders[node]:
                schedule.append((sfc_req, event.job - 1, node))


        return schedule

    def add_instantiation_to_dag(self, sfc_req, vnf):
        i_vnf = VNF(vnf.type, True, vnf.queue_demand)
        index = sfc_req.sfc.vnfs.index(vnf)
        sfc_req.add_vnf(i_vnf, (index+1,))


    def compcost(self, job, node):
        if (job,node) in self.cost:
            return self.cost[(job, node)]
        else:
            return 100000000
    
    def commcost(self, ni, nj, A, B):
        return 1
