#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from network import Network
from node import Node
from sfc import SFC, SFCRequest
from user import User
from vnf import VNF
from rr import RoundRobinScheduler
from tabu import TabuScheduler
from proposed_scheduler import ProposedScheduler

from operator import attrgetter

import simpy
import math
import random
import argparse
import numpy as np

N_SCHEDULERS = 4

vnfs = []
ptime = []
itime = []
schedulers = []
nets = []
nodes = []
sfcs = []
users = []

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
parser.add_argument("lifetime", type=int)
parser.add_argument("n_users", type=int)
args = parser.parse_args()

for scheduler in range(N_SCHEDULERS):
    nodes.append([])

env = simpy.Environment()

for i in range(10):
    queue_demand = random.randint(7, 10)
    vnf = VNF(i, False, queue_demand)
    vnfs.append(vnf)

    
for i in range(50):
    n_vnfs = random.randint(5,10)
    instantiated_vnfs = list(np.random.choice(vnfs, n_vnfs, replace=True))
    ptime = [0]*len(vnfs)
    itime = [0]*len(vnfs)
    dtime = [0]*len(vnfs)
    for vnf in vnfs:
        ptime[vnf.type] = random.randint(15,30)
        itime[vnf.type] = random.randint(5, 10)
        dtime[vnf.type] = random.randint(5, 10)
    queue_size = random.randint(75, 100)
    for scheduler in [0,1]:
        nodes[scheduler].append(Node(i, 8, instantiated_vnfs, n_vnfs, queue_size, ptime, itime, dtime, env))
    nodes[2].append(Node(i, 8, [], n_vnfs, queue_size, ptime, itime, dtime, env))
    nodes[3].append(Node(i, 1, instantiated_vnfs, n_vnfs, queue_size, ptime, itime, dtime, env))

for i in range(5):
    n_vnfs = random.randint(3,5)
    sfc_vnfs = random.sample(vnfs, n_vnfs)
    deadline = random.randint(5000, 10000)
    sfc = SFC(i, sfc_vnfs, deadline)
    sfcs.append(sfc)

schedulers.append(RoundRobinScheduler())
schedulers.append(TabuScheduler(ext=True))
schedulers.append(ProposedScheduler())
schedulers.append(TabuScheduler())

for scheduler in [0,1,2,3]:
    nets.append(Network(nodes[scheduler], sfcs, schedulers[scheduler], env))

    
for i in range(args.n_users):
    req_time = random.randint(5,5)
    user = User(i, args.lifetime, 5, nets, sfcs, env)
    users.append(user)

env.run()

total = 0.0

print('scheduler_name,total_requests,completed_requests,rejected_requests,schedule_time,sfcs,exec_time,requests')

for scheduler in [0,1,2,3]:

    events = ""
    requests = ""
    for i, sfc in enumerate(nets[scheduler].completed_requests):
        if sfc.end_time - sfc.req_time > sfc.sfc.deadline:
            nets[scheduler].rejected_requests.append(sfc)
            del nets[scheduler].completed_requests[i]


    for user in users:
        for request in user.requests:
            requests += repr(request)
            requests += " "

    for node in nets[scheduler].nodes:
        for i, event in enumerate(sorted(node.events, key=attrgetter("end_time"))):
            if event.type == 0:
                events += repr(event)
                events += " "

    events = events[:-1]
    requests = requests[:-1]
    print("%s,%d,%d,%d,%f,\"%s\",%f,\"%s\"" % (schedulers[scheduler].name,
                             len(nets[scheduler].completed_requests)+len(nets[scheduler].rejected_requests),
                             len(nets[scheduler].completed_requests),
                             len(nets[scheduler].rejected_requests),
                             sorted(nets[scheduler].completed_requests,
                                    key=attrgetter("end_time"))[-1].end_time, events, nets[scheduler].exec_time/nets[scheduler].execs, requests))
