# VNF Scheduler

This repository contains the implementation of three VNF scheduler algorithms:

- The `tabu.py` is an implementation of the scheduler proposed by [Mijumbi et al](https://ieeexplore.ieee.org/document/7116120/).
- The `rr.py` is an implementation of a scheduler that uses the Round-Robin approach.
- The `proposed_scheduler.py` is an implementation of the algorithm proposed in [Consideration of software requirements in VNFs scheduling](www.ime.usp.br/~gsantos/vnf_scheduling.pdf)



## Running the code

To run the schedulers, just run in a terminal:

```bash
python3 main.py [time-interval] [n_requests_each_ms]
```

#### Offline Test
To run the offline test, run in a terminal:

```bash
./offline_test.sh
```

This will run 10 times the schedulers in a increasing number of SFC requests. 
The results will be placed in the offline_test directory.

#### Online Test

To run the online test, run in a terminal:

```bash
./online_test.sh
```

This will create 10 processes and run the online test in each of them. After the end, the results will be placed in the online_test directory.

## Plotting the graphs

To plot the graphs of the offline_test, run in a terminal:

```bash
python3 offline_test_graphs.py
``` 

Now, to plot the graph of the online_test, run:

```bash
python3 online_test_graph.py
```

Below we have an example of the online graph:

![completed_vnfs](online_test/completed_vnfs.png)

# Contributing

Feel free to contribute with this project. Considering the little time available 
to implement this code, there are a lot of things to fix and improve. 
Below we have a list of possible improvements to be made:

- [ ] fix program output (we have two fields in the csv that are lists).
- [ ] improve command line interface.
- [ ] create an abstract class to represent the schedulers.
- [ ] modularize the scheduler validity verifier (lines 53-61 `tabu.py` and 31-39 in `rr.py`).
- [ ] improve the scripts to plot the graphs.

# Software/Libraries used in this project

We are using the following software/libraries in this project:

- [Heterogeneous Earliest Finish Time](https://github.com/mrocklin/heft) implementation by [Matthew Rocklin](http://matthewrocklin.com/)
- [Simpy](https://simpy.readthedocs.io/en/latest/): Discrete event simulation for Python 

Thanks!

# License
This program is free software and you can redistribute/modify it under the terms of the [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.html).

