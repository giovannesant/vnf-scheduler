#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import simpy
import copy

class SFCRequest:

    def __init__(self, sfc, req_time, env):
        self.req_time = req_time
        self.sfc = sfc
        self.vnf_ends = []
        self.vnfs = copy.deepcopy(sfc.vnfs)
        self.completed = env.event()
        self.end_time = 0
        self.start_time = 0
        self.env = env
        self.last_index = len(sfc.vnfs)+1
        
        self.dag = {}

        for i in range(sfc.size):
            self.dag[i+1] = (i+2,)
            
        self.dag[sfc.size] = ()
        
        for vnf in sfc.vnfs:
            self.vnf_ends.append(env.event())

    def add_vnf(self, vnf, next):
        self.dag[self.last_index] = next
        self.vnf_ends.append(self.env.event())
        self.vnfs.append(vnf)
        self.last_index += 1

    def previous(self, vnf):
        p = []
        for k,v in self.dag.items():
            if vnf+1 in v:
                p.append(k-1)
        return p

    def was_executed(self):
        for e in self.vnf_ends:
            if e.processed == False:
                return False
        return True
        
    def __repr__(self):
        return "%d,%d,%d" % (len(self.sfc.vnfs), self.req_time, self.req_time + self.sfc.deadline)

class SFC:

    def __init__(self, id, vnfs, deadline):
        self.id = id
        self.vnfs = vnfs
        self.size = len(vnfs)
        self.deadline = deadline

     

