#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import simpy

from lru import LRU

import time
import traceback

class Network:

    def __init__(self, nodes, sfcs, scheduler, env):
        self.nodes = nodes
        self.sfcs = sfcs
        self.scheduler = scheduler
        self.lru = LRU()
        self.requests = simpy.Store(env)
        self.completed_requests = []
        self.rejected_requests = []
        self.total_flow_time = 0
        self.scheduler_cpu = simpy.Resource(env)
        self.exec_time = 0
        self.execs = 0
        
        self.env = env
        self.action = env.process(self.run())

    def run(self):
        while True:
            user, sfc_req, req_time  = yield self.requests.get()

            if (self.env.now) > (sfc_req.sfc.deadline + req_time):
                self.rejected_requests.append(sfc_req)
            else:
                with self.scheduler_cpu.request() as req:
                    yield req
                    try:
                        start = time.time()
                        schedule = self.scheduler.schedule(self, sfc_req)
                        end = time.time()
                        self.exec_time += (end - start)
                        self.execs += 1
                    except:
                        yield self.env.timeout(1)
                        self.requests.put((user, sfc_req, req_time))
                    else:
                        for sfc_req, vnf, node in schedule:
                            node.map(sfc_req, vnf)
                            if sfc_req.vnfs[vnf].instantiation == False:
                                self.lru.insert_item((sfc_req.sfc.vnfs[vnf], node))
                        self.env.process(self.monitor(sfc_req))
                        
    def monitor(self, sfc_req):
        sfc_req.start_time = self.env.now
        yield sfc_req.completed
        sfc_req.end_time = self.env.now
        self.total_flow_time += (sfc_req.end_time - sfc_req.start_time)
        self.completed_requests.append(sfc_req)
                
    def executable_nodes(self, vnf):
        return [node for node in self.nodes if vnf in node.instantiated_vnfs and node.has_resources(vnf)]


    def does_not_fit(self, vnf):
                
        for node in self.nodes:
            if node.has_capacity():
                return False
            
        return True

    def instance_not_available(self, vnf):
        for node in self.nodes:
            if vnf in node.instantiated_vnfs:
                return False
        return True
        
    def destroy_lru(self):
        try:
            vnf, node = self.lru.get()
        except:
            self.nodes[0].destroy(self.nodes[0].instantiated_vnfs[0])
        else:
            self.lru.pop()
            node.destroy(vnf)
