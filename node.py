#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from event import Event

import simpy
import random

from operator import attrgetter

class Node:

    def __init__(self, id, cpus, instantiated_vnfs, capacity, queue_size, ptime, itime, dtime, env):
        self.id = id
        self.instantiated_vnfs = instantiated_vnfs
        self.capacity = capacity
        self.queue_size = queue_size
        self.cur_queue_size = 0
        self.cpu = simpy.Resource(env, cpus)
        self.vnf_instances = []
        for vnf in instantiated_vnfs:
            self.vnf_instances.append((vnf, simpy.Resource(env, 1)))
        self.ptime = ptime
        self.itime = itime
        self.dtime = dtime
        self.queue = simpy.Store(env)
        self.env = env
        self.action = env.process(self.run())
        self.next_start = 0
        self.events = []
        self.update_stats = simpy.Resource(env, 1)

    def run(self):
        while True:
            sfc_req, vnf = yield self.queue.get()

            with self.cpu.request() as req:
                yield req
                self.env.process(self.process(sfc_req, vnf))


    def process(self, sfc_req, vnf):
        for previous_vnf in sfc_req.previous(vnf):
            yield sfc_req.vnf_ends[previous_vnf]
        if sfc_req.vnfs[vnf].instantiation:
            start_time = self.env.now
            yield self.env.process(self.execute(sfc_req, vnf))
            end_time = self.env.now
            self.events.append(Event(1, self, sfc_req.sfc, sfc_req.vnfs[vnf], sfc_req.req_time, start_time, end_time))
        else:
            instances = [instance for f,instance in self.vnf_instances if f == sfc_req.sfc.vnfs[vnf]]
            instance = instances[random.randint(0,len(instances)-1)]
            for i in sorted(instances, key=attrgetter("count")):
                if i.count == 0:
                    instance = i
            with instance.request() as req:
                yield req
                start_time = self.env.now
                yield self.env.process(self.execute(sfc_req, vnf))
                if sfc_req.was_executed():
                    sfc_req.completed.succeed()
                end_time = self.env.now
                self.events.append(Event(0, self, sfc_req.sfc, sfc_req.sfc.vnfs[vnf], sfc_req.req_time, start_time, end_time))
            

    def execute(self, sfc_req, vnf):
        yield self.env.timeout(self.get_time(sfc_req.vnfs[vnf]))
        with self.update_stats.request() as req:
            yield req
            self.cur_queue_size -= sfc_req.vnfs[vnf].queue_demand
            self.next_start -= self.ptime[sfc_req.vnfs[vnf].type]
        sfc_req.vnf_ends[vnf].succeed()

    def get_time(self, vnf):
        if vnf.instantiation == True:
            return self.itime[vnf.type]
        else:
            return self.ptime[vnf.type]
            
    def has_resources(self, vnf):
        if (self.queue_size - self.cur_queue_size) >= vnf.queue_demand:
            return True
        return False

    def has_capacity(self):
        if ((len(self.instantiated_vnfs) + 1) <= self.capacity):
            return True
        return False

    def map(self, sfc_req, vnf):
        self.queue.put((sfc_req,vnf))
        self.cur_queue_size += sfc_req.vnfs[vnf].queue_demand
        self.next_start += self.ptime[sfc_req.vnfs[vnf].type]

    def destroy(self, vnf):
        self.instantiated_vnfs.remove(vnf)

    def instantiate(self, vnf):
        self.instantiated_vnfs.append(vnf)
        self.vnf_instances.append((vnf, simpy.Resource(self.env, 1)))

    def __repr__(self):
        return "node=%d, queue_size=%d" % (self.id, self.cur_queue_size)
