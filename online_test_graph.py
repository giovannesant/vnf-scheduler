#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import csv
import sys
import statistics
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from itertools import cycle

mean, std, mc = {}, {}, {}

mean["RoundRobinScheduler"] = []
std["RoundRobinScheduler"] = []
mean["TabuScheduler"] = []
std["TabuScheduler"] = []
mean["ProposedScheduler"] = []
std["ProposedScheduler"] = []
mean["TabuSchedulerExt"] = []
std["TabuSchedulerExt"] = []
mc[1000], mc[1500], mc[2000], mc[2500] = [], [], [], []

csv.field_size_limit(sys.maxsize)


for it in range(1, 10):
    times = {}
    must_complete = {}
    must_complete[1000], must_complete[1500], must_complete[2000], must_complete[2500] = 0,0,0,0
    times["RoundRobinScheduler"], times["TabuScheduler"], times["ProposedScheduler"], times["TabuSchedulerExt"] = [], [], [], []

    file="./online_test/" + str(it) + ".txt"

    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:

                ranges = row[5].split(' ')
                requests_ranges = row[7].split(' ')
                completed = [0]*523356
                end_times = []

                for r in ranges:
                    start_time, end_time = r.split(',')
                    end_times.append(int(end_time))

                for t in end_times:
                    completed[t] += 1

                times[row[0]].append(np.cumsum(completed))

                for r in requests_ranges:
                    n_vnfs, req_time, end_time = r.split(',')
                    for i in range(1000,3000, 500):
                        if int(req_time) <= i and int(req_time) >= i-500:
                            must_complete[i] += int(n_vnfs)
                            
                for i in range(1000, 3000, 500):
                    mc[i].append(must_complete[i])


for i in range(1000,3000,500):
    mc[i] = int(statistics.mean(mc[i]))

print(mc)
fig, ax = plt.subplots()

ind = np.arange(len(mc))

print(ind)

linestyles = cycle(['^','o','s','p'])

c_vnfs = {}

for scheduler in ["ProposedScheduler", "TabuScheduler", "RoundRobinScheduler", "TabuSchedulerExt"]:
        
    for i in range(3000):
        t = []
        for it in times[scheduler]:
            t.append(it[i])
        mean[scheduler].append(statistics.mean(t))

#    print('Scheduler')
#    for i in range(1000,3000,500):
#        if scheduler not in c_vnfs:
#            c_vnfs[scheduler] = [mean[scheduler][i]-mean[scheduler][i-500]]
#        else:
#            c_vnfs[scheduler].append(mean[scheduler][i]-mean[scheduler][i-500])
    
    #y = []
    #for c in mean[scheduler]:
    #    y.append(c/mean['ProposedScheduler'][-1])
    #    if c/mean['ProposedScheduler'][-1] == 1:
    #        break

        
    ax.plot(list(range(len(mean[scheduler]))), mean[scheduler], label=scheduler, marker=next(linestyles), markevery=max( int(len(mean[scheduler]) / 10), 1))
width = 0.35
x = []
for i in range(1000,3000,500):
    x.append(mc[i])

#ax.bar(ind - width, x, width/2, label='Requested Vnfs')
#ax.bar(ind - width/2, c_vnfs["ProposedScheduler"], width/2, label='ProposedScheduler')
#ax.bar(ind + width, c_vnfs["TabuScheduler"], width/2, label='TabuScheduler')
#ax.bar(ind + width/2, c_vnfs["RoundRobinScheduler"], width/2, label='TabuScheduler')
print('RR', mean['RoundRobinScheduler'][-1],mean['ProposedScheduler'][-1])
print('TB', mean['TabuScheduler'][-1],mean['ProposedScheduler'][-1])


ax.set(xlabel='time (ms)', ylabel='Completed VNFs',
       title='Completed VNFs')
ax.legend(loc='lower right')
plt.show()

