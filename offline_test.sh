#!/bin/bash

mkdir -p offline_test

for it in $(seq 1 10); do
    mkdir -p offline_test/$it
    for n_reqs in $(seq 10 10 100); do
	echo "$it $n_reqs"
	python3 main.py 0 $n_reqs > offline_test/$it/$n_reqs.txt
    done
done
