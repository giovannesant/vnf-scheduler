

class LRU:

    def __init__(self):
        self.item_list = []


    def insert_item(self, item):

        if item in self.item_list:
            index = self.item_list.index(item)
            self.item_list[:] = self.item_list[:index] + self.item_list[1+index:]
            self.item_list.insert(0, item)
        else:
            self.item_list.insert(0, item)

    def get(self):
        return self.item_list[-1]

    def pop(self):
        if len(self.item_list) > 0:
            del self.item_list[-1]
