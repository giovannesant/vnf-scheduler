#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from itertools import cycle

class RoundRobinScheduler:

    def __init__(self):
        self.name = "RoundRobinScheduler"
        self.node_order = []

    def schedule(self, network, sfc_req):
        schedule = []
        
        if self.node_order == []:
            self.node_order = cycle(network.nodes)

        for i, vnf in enumerate(sfc_req.sfc.vnfs):
            tries = 1
            node = next(self.node_order)
            
            while (vnf not in node.instantiated_vnfs) or (not node.has_resources(vnf)):
                tries += 1
                node = next(self.node_order)
                if tries > len(network.nodes):
                    break
            
            if tries > len(network.nodes):
                raise Exception('Network is full')

            schedule.append((sfc_req, i, node))

        queue_demand = {}
        for sfc_req, i, node in schedule:
            if node not in queue_demand:
                queue_demand[node] = 0
            queue_demand[node] += sfc_req.vnfs[i].queue_demand

        for node, demand in queue_demand.items():
            if node.cur_queue_size + demand > node.queue_size:
                raise Exception("Invalid Schedule")

        return schedule
