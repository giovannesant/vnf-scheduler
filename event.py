#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

class Event:

    def __init__(self, type, node, sfc, vnf, req_time, start_time, end_time):
        self.type = type
        self.sfc = sfc
        self.vnf = vnf
        self.req_time = req_time
        self.start_time = start_time
        self.end_time = end_time
        self.node = node

    def __repr__(self):
        if self.type == 0:
            return "%d,%d" % (self.start_time, self.end_time)
        else:
            return "Instantiated(node=%d, sfc=%d, vnf=%d, req=%d, start=%d, end=%d)" % (self.node.id, self.sfc.id, self.vnf.type, self.req_time, self.start_time, self.end_time)
