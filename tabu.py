#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import simpy
import random

from operator import itemgetter

class TabuScheduler:


    def __init__(self, ext=None):
        if ext == None:
            self.name = "TabuScheduler"
        else:
            self.name = "TabuSchedulerExt"
        self.tabu_list = []

    def schedule(self, network, sfc_req):
        solution_set = []
        schedule = self.random_scheduling(network, sfc_req)
        Z = schedule
        m = 0

        if len(schedule) == 0:
            raise Exception('Network is full')

        cur_flow_time = self.flow_time(schedule)
        last_flow_time = self.flow_time(schedule)

        while m <= len(sfc_req.sfc.vnfs):
            greater_flow_time = 0
            neighborhood, moved_vnf = self.get_neighborhood(network, Z, sfc_req)

            if len(neighborhood) == 0:
                break

            for s_candidate in neighborhood:
                node = s_candidate[moved_vnf][2]
                if self.is_not_tabu(moved_vnf, node):
                    solution_set.append(s_candidate)

            s_candidate = self.best_flow_time(solution_set)
            Z = s_candidate
            self.update_tabu_list()


            if self.flow_time(s_candidate) < self.flow_time(schedule):
                self.tabu_list.append((moved_vnf, s_candidate[moved_vnf][2], len(sfc_req.sfc.vnfs)))
                schedule = s_candidate
                last_flow_time = cur_flow_time
                cur_flow_time = self.flow_time(schedule)
                m = 0
            m += 1

        queue_demand = {}
        for sfc_req, i, node in schedule:
            if node not in queue_demand:
                queue_demand[node] = 0
            queue_demand[node] += sfc_req.vnfs[i].queue_demand

        for node, demand in queue_demand.items():
            if node.cur_queue_size + demand > node.queue_size:
                raise Exception("Invalid Schedule")

        return schedule

    def random_scheduling(self, network, sfc_req):

        schedule = []
        for i, vnf in enumerate(sfc_req.sfc.vnfs):
            executable_nodes = network.executable_nodes(vnf)
            if len(executable_nodes) == 0:
                return []

            node = random.choice(executable_nodes)
            schedule.append((sfc_req, i, node))
        return schedule


    def get_neighborhood(self, network, schedule, sfc_req):
        times = []
        time_gaps = []
        neighborhood = []
        start_time = 0

        for i, vnf in enumerate(sfc_req.sfc.vnfs):
            node = schedule[i][2]
            start_time += node.next_start
            end_time = start_time + node.ptime[sfc_req.sfc.vnfs[i].type]
            times.append((start_time, end_time))
            start_time = end_time

        for i, f in enumerate(zip(times[:-1],times[1:])):
            f1, f2 = f
            time_gap = f2[0] - f1[1]
            time_gaps.append((time_gap,i+1))
            
        for candidate in sorted(time_gaps, key=itemgetter(0), reverse=True):
            executable_nodes = network.executable_nodes(sfc_req.sfc.vnfs[candidate[1]])
            if len(executable_nodes) == 0:
                continue
            
            for executable_node in executable_nodes:
                new_schedule = []
                for sfc_req, vnf, node in schedule:
                    if vnf != candidate[1]:
                        new_schedule.append((sfc_req, vnf, node))
                    else:
                        new_schedule.append((sfc_req, vnf, executable_node))
                neighborhood.append(new_schedule)

            return (neighborhood, candidate[1])

    def is_not_tabu(self, moved_vnf, node):
        for m_vnf, n, m in self.tabu_list:
            if m_vnf == moved_vnf and n == node:
                return False
        return True

    def update_tabu_list(self):
        self.tabu_list[:] = [(tabu[0],tabu[1],tabu[2]-1) for tabu in self.tabu_list if tabu[2]-1 > 0 ]


    def best_flow_time(self, solution_set):
        times = []
        for schedule in solution_set:
            f = self.flow_time(schedule)
            times.append((schedule, f))

        best_flow_time = sorted(times, key=itemgetter(1))
        return best_flow_time[0][0]


    def flow_time(self, schedule):
        first_node = schedule[0][2]
        start_time = first_node.next_start
        end_time = 0

        for sfc_req, vnf, node in schedule:
            start = node.next_start
            end_time += node.ptime[sfc_req.sfc.vnfs[vnf].type] + start
            
        diff = (end_time - start_time)
        return diff
