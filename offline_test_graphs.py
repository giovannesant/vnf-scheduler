#    Copyright (C) 2019  Giovanne Marcelo dos Santos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import csv
import statistics
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

mean, std = {}, {}
exec_time = {}

mean["RoundRobinScheduler"], exec_time["RoundRobinScheduler"] = [], []
std["RoundRobinScheduler"] = []
mean["TabuSchedulerExt"], exec_time["TabuSchedulerExt"] = [], []
std["TabuSchedulerExt"] = []
mean["ProposedScheduler"], exec_time["ProposedScheduler"] = [], []
std["ProposedScheduler"] = []
mean["TabuScheduler"], exec_time["TabuScheduler"] = [], []
std["TabuScheduler"] = []

for n_reqs in range(10,110,10):
    times = {}
    exec_times = {}
    times["RoundRobinScheduler"], times["TabuScheduler"], times["ProposedScheduler"], times["TabuSchedulerExt"] = [], [], [], []
    exec_times["RoundRobinScheduler"], exec_times["TabuScheduler"], exec_times["ProposedScheduler"], exec_times["TabuSchedulerExt"] = [], [], [], []

    for it in range(1,11):
        file="./offline_test/" + str(it) + "/" + str(n_reqs) + ".txt"

        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')

            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    times[row[0]].append(float(row[4]))
                    exec_times[row[0]].append(float(row[6])*1000)

    for scheduler in ["RoundRobinScheduler", "TabuScheduler", "ProposedScheduler", "TabuSchedulerExt"]:
        mean[scheduler].append(statistics.mean(times[scheduler]))
        exec_time[scheduler].append(statistics.mean(exec_times[scheduler]))
        std[scheduler].append(statistics.stdev(times[scheduler]))



ind = np.arange(len(mean["ProposedScheduler"]))
width = 0.80


plt.figure(0)
fig, ax = plt.subplots()
rects1 = ax.plot(ind, mean["ProposedScheduler"], marker='^', label="ProposedScheduler")
rects2 = ax.plot(ind, mean["TabuScheduler"], marker='o', label="TabuScheduler")
rects3 = ax.plot(ind, mean["RoundRobinScheduler"], marker='s', label="RoundRobinScheduler")
rects4 = ax.plot(ind, mean["TabuSchedulerExt"], marker='p', label="TabuSchedulerExt")

ax.set_ylabel('Schedule Time (ms)')

ax.set_xlabel('Number of SFC requests')
ax.set_title('Offline Test')
ax.set_xticks(ind)
ax.set_xticklabels(list(range(10,110,10)))
ax.legend(loc='upper left')


print(mean["TabuSchedulerExt"][-1], mean["ProposedScheduler"][-1], mean["RoundRobinScheduler"][-1])

plt.figure(1)
fig, ax = plt.subplots()

means = []
stds = []
for scheduler in ["TabuScheduler", "ProposedScheduler"]:
    means.append(statistics.mean(exec_times[scheduler]))
    stds.append(statistics.stdev(exec_times[scheduler]))

ax.bar(np.arange(2), means, yerr=stds)

ax.set_ylabel('Execution Time (ms)')
ax.set_xlabel('Schedulers')
ax.set_title('Execution Time')
ax.set_xticks(np.arange(2))
ax.set_xticklabels(["TabuScheduler", "ProposedScheduler"])

print(means[0], means[1])

plt.show()
        
