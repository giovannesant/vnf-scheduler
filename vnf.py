
class VNF:

    def __init__(self, type, instantiation, queue_demand, destroy=None):
        self.type = type
        self.instantiation = instantiation
        if destroy == None:
            self.destroy = False
        else:
            self.destroy = True
        self.queue_demand = queue_demand


